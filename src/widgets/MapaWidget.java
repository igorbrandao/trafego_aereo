package widgets;

import context.arch.storage.Attribute;
import context.arch.storage.AttributeNameValue;
import context.arch.widget.Widget;

public class MapaWidget extends Widget {

	/**
	 * Atributos do mapa
	 */
	private String nome;	// (América do Sul, Europa, Ásia, Oceania etc...)
	private double width;
	private double height;
	
	public static final String CLASSNAME = MapaWidget.class.getName();
	
	/**
	 * Construtor do mapa
	 */
	public MapaWidget(String nome, double width, double height) {
		
		// Chama o construtor da classe widget
		super(nome, CLASSNAME);
		
		// Inicializa os atributos
		this.nome = nome;
		this.width = width;
		this.height = height;
		
		// Chama o método start de widget
		super.start(true);
	}
	
	@Override
	/*
	 * Inicializa o MapaWidget
	 */
	protected void init() {
		// non-constant attributes
		addAttribute(Attribute.instance("nome", String.class));
		addAttribute(Attribute.instance("width", Double.class));
		addAttribute(Attribute.instance("height", Double.class));
		
		// constant attributes
		//addAttribute(AttributeNameValue.instance(ROOM, room), true);
	}
}