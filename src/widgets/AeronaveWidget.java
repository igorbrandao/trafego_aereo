package widgets;

import context.arch.storage.Attribute;
import context.arch.storage.AttributeNameValue;
import context.arch.widget.Widget;

public class AeronaveWidget extends Widget {

	/**
	 * Atributos da aeronave
	 */
	private String 	nome;			// identificador da aeronave (precisa ser único)
	private double 	altura;
	private double 	velocidade;
	private int[] 	localizacao;
	private int[][] coordenadas;	// conjunto coordenadas (x, y)
	
	public static final String CLASSNAME = AeronaveWidget.class.getName();
	
	/**
	 * Construtor da aeronave
	 */
	public AeronaveWidget(String nome, double altura, double velocidade, int[] localizacao, int[][] coordenadas) {
		
		// Chama o construtor da classe widget
		super(nome, CLASSNAME);
		
		// Inicializa os atributos
		this.nome = nome;
		this.altura = altura;
		this.velocidade = velocidade;
		this.localizacao = localizacao;
		this.coordenadas = coordenadas;
		
		// Chama o método start de widget
		super.start(true);
	}
	
	@Override
	/*
	 * Inicializa o AeronaveWidget
	 */
	protected void init() {
		// non-constant attributes
		addAttribute(Attribute.instance("nome", String.class));
		addAttribute(Attribute.instance("altura", Double.class));
		addAttribute(Attribute.instance("velocidade", Double.class));
		addAttribute(Attribute.instance("proximo_ponto", Integer.class));
		
		// constant attributes
		//addAttribute(AttributeNameValue.instance(ROOM, room), true);
	}
}